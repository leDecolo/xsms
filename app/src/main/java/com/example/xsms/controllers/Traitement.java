package com.example.xsms.controllers;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import java.util.HashMap;
import java.util.Map;

public class Traitement {
    public Map<String, String> Mescontacts;
    public Traitement(Context context){
        // notre tableau de contact
        Mescontacts = new HashMap<>();
        // instance qui permet d'acceder au contenu d'autre application
        ContentResolver ConnectApp = context.getContentResolver();
        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] projection = new String[] {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME_ALTERNATIVE
                , ContactsContract.CommonDataKinds.Phone.NUMBER};
        // on récupere les contacts dans un curseur
        Cursor cursor = ConnectApp.query(uri, projection, null, null, null);

        if (cursor.moveToFirst()) {
            do {
                String num = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)).replace(" ", "");
                num = num.replaceAll("[+]237", "");
                Mescontacts.put(num, cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME_ALTERNATIVE)));
            } while (cursor.moveToNext());
        }
    }
}
