package com.example.xsms;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class NewChatActivity extends AppCompatActivity {

    ImageButton sendBtn;
    EditText number, sendMsg;
    DatabaseHelper myDB;
    Spinner spin;
    ArrayAdapter<String> adapter;
    int spinnerPosition;
    Map<String, String> contact_num;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_chat);

        sendMsg = findViewById(R.id.SendMessage);
         number = findViewById(R.id.newNumber);
         sendBtn = findViewById(R.id.SendBtn);

        contact_num = new HashMap<>();
         spin = findViewById(R.id.idspinner);

        adapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, MainActivity.numero_nom);
        spin.setAdapter(adapter);


        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //ici je recupere la position de l'element selectionne
                spinnerPosition = ((ArrayAdapter)spin.getAdapter()).getPosition("contact");
                // maintenant je me rassure d'abord que dans ma liste de contact je ne selectionne pas "contact"
                if(!spin.getSelectedItem().toString().equals("contact")){
                    // je casse le contacte selectionne car il est sous la forme (nom-numero)
                    String[] nom = spin.getSelectedItem().toString().split("-");
                    System.out.println("nom: "+nom[0]+" numero: "+nom[1]);
                    // j'insere maintenant cela dans ma liste de contact en me rassurant d'abord que le text field qui affiche les contacts est soit vide ou pas
                    if(number.getText().toString().equals("")){
                        number.setText(nom[0]);
                        contact_num.put(nom[0], nom[1]);
                        spin.setSelection(spinnerPosition);
                    } else {
                        number.setText(number.getText().toString() + " ; " + nom[0]);
                        contact_num.put(nom[0], nom[1]);
                        spin.setSelection(spinnerPosition);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //send the sms
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] nom1 = number.getText().toString().split(";");
                if (sendMsg.getText().toString().length() < 160) {
                    // j'initialise les elements qui vont me permettre d'etablire l'accuse de reception
                    // l'objet qui vq me dire si le message a ete envoye
                    PendingIntent sentPI = PendingIntent.getBroadcast(getApplicationContext(), 0, new Intent("SMS_ENVOYE"), 0);

                    //l'objet qui va me dire si le recepteur a recu le message
                    PendingIntent deliveredPI = PendingIntent.getBroadcast(getApplicationContext(), 0, new Intent("SMS_RECU"), 0);
                    // j'initialise un compteur pour pouvoir pointe a chaque fois le nom de la personne dont j'utilise son numero et je prend ces noms dans ma liste de numero (num_tel)
                    int i = 0;
                    //
                    // ici j'envoi le message proprement dit en parcourant la liste des numero
                    // String[] contact = num_tel.getText().toString().split(";");
                    for (String num1 : nom1) {
                        String num = contact_num.get(num1.trim());
                        if (num == null)
                            num = num1.trim();

                        // j'envoi le message

                        boolean send = SmsSender.sendSMS(num,sendMsg.getText().toString());
                        if(send) {
                            Toast.makeText(getBaseContext(), "Sent message", Toast.LENGTH_SHORT).show();
                            number.setText("");
                            sendMsg.setText("");
                        }
                    }


                    //nombre de message superieur a 160
                } else {

                    // j'initialise les elements qui vont me permettre d'etablire l'accuse de reception
                    // l'objet qui vq me dire si le message a ete envoye
                    ArrayList<PendingIntent> sentPendingIntents = new ArrayList<>();

                    //l'objet qui va me dire si le recepteur a recu le message
                    ArrayList<PendingIntent> deliveredPIs = new ArrayList<PendingIntent>();                // j'initialise un compteur pour pouvoir pointe a chaque fois le nom de la personne dont j'utilise son numero et je prend ces noms dans ma liste de numero (num_tel)
                    int i = 0;
                    //
                    // ici j'envoi le message proprement dit en parcourant la liste des numero
                    for (String num1 : nom1) {

                        String num = contact_num.get(num1.trim());
                        if (num == null)
                            num = num1.trim();
                        SmsManager smsmanager = SmsManager.getDefault();
                        // j'envoi le message
                        ArrayList<String> parts = smsmanager.divideMessage(sendMsg.getText().toString());
                        smsmanager.sendMultipartTextMessage(num, null, parts, sentPendingIntents, deliveredPIs);
                    }

                }



                    boolean send = SmsSender.sendSMS(number.getText().toString(),sendMsg.getText().toString());
                    if(send) {
                        Toast.makeText(getBaseContext(), "Sent message", Toast.LENGTH_SHORT).show();
                        number.setText("");
                        sendMsg.setText("");
                    }
                    else
                        Toast.makeText(getBaseContext(), "Not Sent", Toast.LENGTH_SHORT).show();


            }
        });


    }






}
